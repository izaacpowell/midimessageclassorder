//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>

class MidiMessage
{
    public:
        MidiMessage() //constructor
    {
        number = 60;
        velocity = 60;

    }
        ~MidiMessage() //Destructor
    {
        std::cout << "Destructed:\n";
    }
    void setNoteNumber (int value) //Mutator
    {
        number = value;
    }
    int getNoteNumber() const //Accessor
    {
    return number;
    }
    
    float getMidiNoteInHertz() const //Accessor
    {
        return 440 * pow(2, (number-69) / 12.0);
    }
    
    void setFloatVelocity (int value) const
    {
   
    }
    
    
    float getFloatVelocity() const //Izaac the don
    {
        return velocity / 127.0;
    }
    
private:
    int number;
    int velocity;
    float amplitude;
};

int main (int argc, const char* argv[])
{

    // insert code here...
    MidiMessage note;
    note.setNoteNumber(77 / 6);
    std::cout << "Note Number:\n" << note.getNoteNumber() << std::endl;
    
    
    //std::cout << "Hello, World!\n";

    
    return 0;
}

